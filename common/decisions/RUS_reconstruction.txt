RUS_reconstruction_category = {
	
	RUS_start_reconstruction_phase = {
		allowed = { is_russian_nation = yes }
		visible = {
			is_debug = yes
		}
		complete_effect = {
			RUS_initiate_reconstruction = yes
		}
	}
	
	RUS_muscovy_invest_into_local_businesses = {
		allowed = { is_russian_nation = yes }
		icon = GFX_decision_generic_money_sign
		custom_cost_trigger = { check_variable = { GDP > 0 } }
		custom_cost_text = decision_cost_250_million
		days_remove = 90
		visible = {
			check_variable = { rus_reconstruction_state_selector = 1 }
		}
		complete_effect = {
			RUS_reconstruction_cost_250M = yes
		}
		remove_effect = {
			add_to_variable = { rus_muscovy_GDP = 0.05 }
			add_to_variable = { rus_muscovy_GDP_growth = 0.5 }
			custom_effect_tooltip = RUS_reconstruction_GDP_growth_increase_tt
			custom_effect_tooltip = RUS_reconstruction_GDP_increase_small_tt
		}
	}
	
	RUS_muscovy_build_schools = {
		allowed = { is_russian_nation = yes }
		icon = GFX_decision_build_schools
		custom_cost_trigger = { check_variable = { GDP > 0 } }
		custom_cost_text = decision_cost_500_million
		visible = {
			check_variable = { rus_reconstruction_state_selector = 1 }
		}
		complete_effect = {
			RUS_reconstruction_cost_500M = yes
		}
		remove_effect = {
			add_to_variable = { rus_muscovy_literacy_growth = 0.30 }
			add_to_variable = { rus_muscovy_integration_growth = 0.10 }
			custom_effect_tooltip = RUS_reconstruction_literacy_increase_tt
			custom_effect_tooltip = RUS_reconstruction_integration_increase_small_tt
			TNO_improve_academic_base_low = yes
		}
	}
	
	RUS_muscovy_industrialization_project = {
		allowed = { is_russian_nation = yes }
		icon = GFX_decision_generic_construction
		custom_cost_trigger = { check_variable = { GDP > 0 } }
		custom_cost_text = decision_cost_300_million
		days_remove = 240
		visible = {
			check_variable = { rus_reconstruction_state_selector = 1 }
		}
		available = {
			num_of_civilian_factories_available_for_projects > 9
		}
		complete_effect = {
			RUS_reconstruction_cost_300M = yes
		}
		modifier = {
			civilian_factory_use = 10
		}
		remove_effect = {
			random_owned_state = {
				limit = {
					hidden_trigger = {
						is_core_of = MCW
					}
				}
				add_building_construction = {
						type = industrial_complex
						level = 3
						instant_build = yes
				}
			}
		}
	}
	
	RUS_muscovy_clean_up_farmlands = {
		allowed = { is_russian_nation = yes }
		icon = GFX_decision_industrial_farm
		custom_cost_trigger = { check_variable = { GDP > 0 } }
		custom_cost_text = decision_cost_400_million
		days_remove = 180
		visible = {
			check_variable = { rus_reconstruction_state_selector = 1 }
		}
		complete_effect = {
			RUS_reconstruction_cost_400M = yes
		}
		remove_effect = {
			TNO_improve_agriculture_low = yes
			add_to_variable = { rus_muscovy_unemployment_growth = 0.25 }
			add_to_variable = { rus_muscovy_GDP_growth = 0.25 }
			custom_effect_tooltip = RUS_reconstruction_unemployment_decrease_tt
			custom_effect_tooltip = RUS_reconstruction_GDP_growth_increase_tt
		}
	}
	
	RUS_the_western_highway = {
		allowed = { is_russian_nation = yes }
		icon = GFX_decision_generic_construction
		cost = 75
		visible = {
			check_variable = { rus_reconstruction_state_selector = 1 }
		}
		complete_effect = {
			country_event = { id = rus_reconstruction.1 }
			custom_effect_tooltip = RUS_reconstruction_special_project_tt
		}
	}
	
	RUS_restore_moscow = {
		allowed = { is_russian_nation = yes }
		icon = GFX_decision_generic_construction
		custom_cost_trigger = { check_variable = { GDP > 0 } }
		custom_cost_text = decision_cost_750_million
		visible = {
			check_variable = { rus_reconstruction_state_selector = 1 }
		}
		available = {
			num_of_civilian_factories_available_for_projects > 11
		}
		days_remove = 120
		modifier = {
			civilian_factory_use = 12
			production_speed_buildings_factor = -0.05
		}
		complete_effect = {
			RUS_reconstruction_cost_750M = yes
		}
		remove_effect = {
			hidden_effect = {
				219 = {
					RUS_restore_moscow = yes
				}
				1314 = {
					RUS_restore_moscow = yes
				}
			}
			if = {
				limit = {
					219 = {
						has_dynamic_modifier = {
							modifier = RUS_moscow_in_ruins_5
						}
					}
				}
				custom_effect_tooltip = RUS_reconstruction_moscow_restoration_complete_tt
			}
			else = {
				custom_effect_tooltip = RUS_reconstruction_moscow_restoration_tt
			}
			add_stability = 0.025
		}
	}
}