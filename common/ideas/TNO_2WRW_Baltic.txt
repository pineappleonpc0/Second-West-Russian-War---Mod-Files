ideas = {
	country = {
		LAT_cultural_rebirth = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea LAT_cultural_rebirth"}
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			picture = latvian_cultural_renaissance
			
			modifier = {
				MONTHLY_POPULATION = 0.1
				stability_factor = 0.1
			}
		}
		
		EST_lingustic_classes = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea EST_lingustic_classes"}
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			picture = generic_research_bonus
			
			modifier = {
				research_speed_factor = 0.05
				stability_factor = 0.1
			}
		}
	}
}