ideas = {
	country = {
		PET_reconquest_of_moskowien = { #add this with an event in case of collapse
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VLG_wolga_german_settler_conflict" }
			removal_cost = -1
			picture = OST_Prussian_Values
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			modifier = {
				command_power_gain_mult = 0.25
				army_org_Factor = 0.1
				army_attack_factor = 0.1
			}
		}

		PET_an_army_with_a_state = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VLG_wolga_german_settler_conflict" }
			removal_cost = -1
			picture = DER_Preparing_The_Militias
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			modifier = {
				experience_gain_army = 0.05
				consumer_goods_factor = 0.1
			}
		}		
		
		PET_Defenders_of_the_Reich = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea PET_Defenders_of_the_Reich" }
			removal_cost = -1
			picture = PET_Defenders_of_the_Reich
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			modifier = {
				army_core_defence_factor = 0.25 
				surrender_limit = 0.5
			}
		}		
		
		PET_The_Spectre_of_Schorner = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea PET_The_Spectre_of_Schorner" }
			removal_cost = -1
			picture = PET_The_Spectre_of_Schorner
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			modifier = {
				land_reinforce_rate = 0.2
				stability_factor = -0.10
			}
		}		
		
		PET_Moskowien_Refugee_Crisis = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea PET_Moskowien_Refugee_Crisis" }
			removal_cost = -1
			picture = ORE_refugee_crisis
			
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			modifier = {
				stability_factor = -0.10 
				conscription_factor = 0.2
			}
		}
	}
}