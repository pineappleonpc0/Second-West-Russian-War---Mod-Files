ideas = {
	country = {
		RUS_salted_fields_of_moscow = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea RUS_salted_fields_of_moscow"}
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = OMS_Fueled_by_Revenge
			modifier = {
				consumer_goods_factor = 0.5
				production_speed_buildings_factor = -0.2
				industry_free_repair_factor = 0.5
			}
		}
		
		RUS_burnt_breadbasket = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea RUS_burnt_breadbasket"}
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = OMS_Fueled_by_Revenge
			modifier = {
				consumer_goods_factor = 0.2
				production_speed_buildings_factor = -0.4
				industry_free_repair_factor = 0.5
			}
		}
		
		RUS_ruined_model_colony = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea RUS_ruined_model_colony"}
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = OMS_Fueled_by_Revenge
			modifier = {
				consumer_goods_factor = 0.1
				production_speed_buildings_factor = 0.1
				industry_free_repair_factor = 0.6
			}
		}
		
		RUS_dried_wells_of_caucasus = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea RUS_dried_wells_of_caucasus"}
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = OMS_Fueled_by_Revenge
			modifier = {
				consumer_goods_factor = 0.15
				fuel_gain = -0.4
				industry_free_repair_factor = 0.3
			}
		}
		
		RUS_national_reconstruction = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea RUS_national_reconstruction"}
			allowed = {
				always = no
			}
            cancel = { has_country_flag = RUS_cancel_all_ideas }
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = OMS_Fueled_by_Revenge
			modifier = {
				consumer_goods_factor = 0.3
				fuel_gain = -0.2
				industry_free_repair_factor = 0.5
			}
		}
	
		
	}
}