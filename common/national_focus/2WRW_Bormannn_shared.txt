focus_tree = {
	id = bormannn_tree
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = GER
		}
	}
	default = no
	shared_focus = GER_the_brown_kaiser
}