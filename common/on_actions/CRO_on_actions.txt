on_actions = {
	on_declare_war = {
		effect = {
			if = {
				limit = {
					ROOT = {
						original_tag = ITA
					}
					FROM = {
						has_cosmetic_tag = YUG
					}
				}
				set_country_flag = CRO_war_started
				mark_focus_tree_layout_dirty = yes
				country_event = { id = cro.4 days = 90 }
			}
		}
	}
	on_peace = {
		effect = {
			if = {
				limit = {
					THIS = {
						original_tag = CRO
					}
					has_country_flag = CRO_war_started
				}
				clr_country_flag = CRO_war_started
				mark_focus_tree_layout_dirty = yes
				set_country_flag = CRO_won_against_italy
				create_faction = CRO_liberation_front
			}
		}
	}
}