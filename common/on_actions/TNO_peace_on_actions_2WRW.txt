on_actions = {
	#### PEACE/WAR VICTORY ON_CAPITULATION BLOCK ####
	#### PUT YOUR SHIT HERE
	# At the end of each global block, set the skip_default_capitulation GLOBAL flag
	# Otherwise shit will just get annexed
	# Really
	# It will.

	# FROM = winner
	# ROOT = country being capitulated/loser

	#See TNO_economy_scripted_effects regarding how transfer_GDP works
	on_capitulation = {
		effect = {
			log = "[GetDateText]: FROM: [From.GetName] ROOT: [Root.GetName] on_capitulation"

			set_global_flag = show_peace_popup_alert
			FROM = {
				save_global_event_target_as = winning_country
			}
			ROOT = {
				save_global_event_target_as = losing_country
			}

			log = "[GetDateText]: winner/loser: FROM: [winning_country.GetName] ROOT: [losing_country.GetName] on_capitulation"

			if = {
				limit = {
					event_target:losing_country = {
						any_allied_country = {
							has_war_together_with = event_target:losing_country
						}
					}
				}
				set_global_flag = losing_country_multiple
			}

			if = {
				limit = {
					event_target:winning_country = {
						any_allied_country = {
							has_war_together_with = event_target:winning_country
						}
					}
				}
				set_global_flag = winning_country_multiple
			}

			#Annexation block
			#This if block will only fire if FROM (capitulator) and ROOT (country being capitulated)
			#That is to say, if one side has completely lost (nobody left to fight on)
			if = {
				limit = {
					NOT = {
						ROOT = {
							is_in_faction = yes
							any_other_country = {
								is_in_faction_with = PREV
								has_capitulated = no
							}
						}
					}
				}
				set_global_flag = war_won

				#Transfer 60% of every capitulated country's GDP to the winner
				every_country = {
					limit = {
						OR = {
							is_in_faction_with = ROOT
							original_tag = ROOT
						}
					}
					FROM = {
						#transfer_GDP = yes
					}
				}
				
				# Mountain War
				if = {
					limit = {
						FROM = { is_russian_nation = yes }
						ROOT = { original_tag = TUR }
					}
					FROM = {
						white_peace = ROOT
						if = {
							limit = {
								country_exists = GEO
								country_exists = AZR
							}
							GEO = { transfer_state = 783 }
							ARM = { transfer_state = 230 }
							
							RUS = { puppet = ARM }
						}
						else_if = {
							limit = {
								country_exists = CAU
								NOT = {
									country_exists = GEO
									country_exists = AZR
								}
							}
							CAU = {
								transfer_state = 783
								transfer_state = 230
							}
						}
						else = {
							RUS = {
								transfer_state = 783
								transfer_state = 230
							}
						}
						GRE = {
							transfer_state = 184
							transfer_state = 1266
						}
						
						if = {
							limit = {
								country_exists = WCH
								country_exists = KIR
							}
							transfer_state = 341
						}
						
						# Peace Deal 3
						
						if = {
							limit = {
								NOT = {
									country_exists = SYR
								}
							}
							SYR = {
								transfer_state = 1242
								transfer_state = 1241
								transfer_state = 1240
								transfer_state = 1371
								transfer_state = 1370
								transfer_state = 677
								transfer_state = 1243
								transfer_state = 1369
								transfer_state = 1372
								transfer_state = 554
								transfer_state = 680
							}
							RUS = { puppet = SYR }
						}
						
						if = {
							limit = {
								NOT = {
									country_exists = KUR
								}
							}
							KUR = {
								transfer_state = 781
								transfer_state = 1334
							}
							RUS = { puppet = KUR }
						}
						
						RUS = { puppet = TUR }
					}
					set_global_flag = skip_default_capitulation
					clr_global_flag = TUR_mountain_war
				}
				
				# Liberation of East Turkestan
				if = {
					limit = {
						FROM = { is_russian_nation = yes }
						ROOT = { original_tag = SIK }
					}
					FROM = {
						white_peace = ROOT
						
						ETR = {
							transfer_state = 617
							transfer_state = 618
							transfer_state = 957
							transfer_state = 958
							transfer_state = 619
							add_state_core = 617
							add_state_core = 618
							add_state_core = 957
							add_state_core = 958
							add_state_core = 619
						}
						
						RUS = { puppet = ETR }
					}
					set_global_flag = skip_default_capitulation
				}
				
				# Romania
				if = {
					limit = {
						FROM = { is_russian_nation = yes }
						ROOT = { original_tag = ROM }
					}
					FROM = {
						white_peace = ROOT
						
						ROM = {
							transfer_state = 1281
							transfer_state = 1282
							transfer_state = 46
							transfer_state = 81
							transfer_state = 84
							transfer_state = 1283
							transfer_state = 79
							transfer_state = 1284
							transfer_state = 80
							transfer_state = 78
							transfer_state = 1286
							transfer_state = 1289
							transfer_state = 1288
							transfer_state = 76
							transfer_state = 83
							transfer_state = 82
							transfer_state = 1285
							transfer_state = 1287
						}
						
						RUS = { puppet = ROM }
						
						if = {
							limit = {
								country_exists = UKR
								UKR = {
									is_puppet_of = RUS
								}
							}
							UKR = {
								transfer_state = 192
								transfer_state = 198
								add_state_core = 192
								add_state_core = 198
							}
						}
						
						else = {
							RUS = {
								transfer_state = 192
								transfer_state = 198
								transfer_state = 1285
								add_state_core = 192
								add_state_core = 198
								add_state_core = 1285
							}
						}
						
						if = {
							limit = {
								country_exists = SER
							}
							SER = {
								transfer_state = 968
							}
						}
						
						ROM = {
							add_ideas = CSTO_Member
						}
						
						every_country = {
							news_event = { id = WORLD.40032 days = 1 }
						}
					}
					set_global_flag = skip_default_capitulation
				}
				
				# West China War
				if = {
					limit = {
						FROM = { original_tag = XSM }
						ROOT = { original_tag = TIB }
					}
					FROM = {
						white_peace = ROOT
						
						XSM = {
							transfer_state = 322
							transfer_state = 601
							transfer_state = 765
							add_state_core = 322
							add_state_core = 601
							add_state_core = 765
						}
						
						
						XSM = {
							country_event = { id = PAV_failure.18 days = 1 }
						}
					}
					set_global_flag = skip_default_capitulation
				}
				if = {
					limit = {
						FROM = { original_tag = XSM }
						ROOT = { original_tag = YAA }
					}
					FROM = {
						white_peace = ROOT
						
						XSM = {
							transfer_state = 905
							transfer_state = 964
							add_state_core = 905
							add_state_core = 964
						}
						
						
						XSM = {
							country_event = { id = PAV_failure.18 days = 1 }
						}
					}
					set_global_flag = skip_default_capitulation
				}
				if = {
					limit = {
						FROM = { original_tag = XSM }
						ROOT = { original_tag = SIK }
					}
					FROM = {
						white_peace = ROOT
						
						XSM = {
							transfer_state = 617
							transfer_state = 618
							transfer_state = 957
							transfer_state = 958
							transfer_state = 619
							add_state_core = 617
							add_state_core = 618
							add_state_core = 957
							add_state_core = 958
							add_state_core = 619
						}
						
						
						XSM = {
							country_event = { id = PAV_failure.18 days = 1 }
						}
					}
					set_global_flag = skip_default_capitulation
				}
				#Mongolia
				if = {
					limit = {
						FROM = { original_tag = MON }
						ROOT = { original_tag = MEN }
					}
					FROM = {
						white_peace = ROOT
						
						MON = {
							transfer_state = 815
							add_state_core = 815
						}
						MON = {
							add_ideas = CSTO_Member
						}
						RUS = { add_to_faction = MON }
					}
					clr_global_flag = Mongolian_Uprising_RUS
					set_global_flag = skip_default_capitulation
				}
				
				#Krakow Uprising
				if = {
					limit = {
						FROM = { original_tag = POL }
						ROOT = { original_tag = GGN }
						has_global_flag = RUS_krakow_uprising
					}
					FROM = {
						white_peace = ROOT
						
						POL = {
							transfer_state = 1384
							transfer_state = 1380
							transfer_state = 10
							transfer_state = 1381
							transfer_state = 1383
							transfer_state = 90
							transfer_state = 1382
							transfer_state = 92
						}
						RUS = { add_to_faction = POL }
						POL = {
							add_ideas = CSTO_Member
						}
					}
					clr_global_flag = RUS_krakow_uprising
					set_global_flag = skip_default_capitulation
				}
				if = {
					limit = {
						FROM = { original_tag = GGN }
						ROOT = { original_tag = POL }
						has_global_flag = RUS_krakow_uprising
					}
					FROM = {
						white_peace = ROOT
						
						GGN = {
							transfer_state = 88
							transfer_state = 1385
							transfer_state = 1386
							transfer_state = 1389
							transfer_state = 1390
							transfer_state = 1391
							transfer_state = 1387
							transfer_state = 1392
							transfer_state = 1393
							transfer_state = 91
							transfer_state = 94
							transfer_state = 89
							transfer_state = 1388
						}
					}
					clr_global_flag = RUS_krakow_uprising
					set_global_flag = skip_default_capitulation
				}
				#Capitulation block
				set_global_flag = war_continuing
			}
			clr_global_flag = skip_default_capitulation
		}
	}
	#don't mind me just logging wars
	on_declare_war = {
		effect = {
			log="[GetDateText]: [Root.GetName] declared war on [From.GetName]"
		}
	}
}
