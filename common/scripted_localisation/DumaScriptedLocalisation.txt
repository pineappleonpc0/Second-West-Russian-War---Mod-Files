defined_text = {
	name = GetDumaNameFederation
	text = {
		#trigger = { has_country_flag = PAV_EMPOWERED_DUMA }
		localization_key = PAV_Federation_Duma
	}
}

defined_text = {
	name = GetDumaNameFederationDesc
	text = {
		#trigger = { has_country_flag = Decembrist_Constitution }
		localization_key = PAV_Federation_Duma_desc
	}
}

defined_text = {
	name = PAV_RAPP_Get_RepTotal
	text = {
		trigger = {
			set_temp_variable = { PAV_RAPP_Display_Total = PAV_RAPP_lower_seats }
			add_to_temp_variable = { PAV_RAPP_Display_Total = PAV_RAPP_upper_seats }
			set_temp_variable = { PAV_SIL_Display_Total = PAV_SIL_lower_seats }
			add_to_temp_variable = { PAV_SIL_Display_Total = PAV_SIL_upper_seats }
			check_variable = { PAV_RAPP_Display_Total > PAV_SIL_Display_Total }
		}
		localization_key = "[?PAV_RAPP_Display_Total|G0]"
	}
	text = {
		localization_key = "[?PAV_RAPP_Display_Total|Y0]"
	}
}

defined_text = {
	name = PAV_SIL_Get_RepTotal
	text = {
		trigger = {
			set_temp_variable = { PAV_SIL_Display_Total = PAV_SIL_lower_seats }
			add_to_temp_variable = { PAV_SIL_Display_Total = PAV_SIL_upper_seats }
			set_temp_variable = { PAV_RAPP_Display_Total = PAV_RAPP_lower_seats }
			add_to_temp_variable = { PAV_RAPP_Display_Total = PAV_RAPP_upper_seats }
			check_variable = { PAV_SIL_Display_Total > PAV_RAPP_Display_Total }
		}
		localization_key = "[?PAV_SIL_Display_Total|G0]"
	}
	text = {
		localization_key = "[?PAV_SIL_Display_Total|Y0]"
	}
}

defined_text = {
	name = PAVGetLowerHouseName
	text = {
		trigger = { has_country_flag = Humanist_Constitution }
		localization_key = PAV_RAPP_lower_house_name
	}
	text = {
		localization_key = PAV_RAPP_lower_house_name
	}
}

defined_text = {
	name = PAV_RAPP_Get_PopsTotal
	text = {
		trigger = {
			check_variable = { PAV_RAPP_General_Pop > PAV_SIL_General_Pop }
		}
		localization_key = "PAV_RAPP_General_Pop_G"
	}
	text = {
		localization_key = "PAV_RAPP_General_Pop_Y"
	}
}

defined_text = {
	name = PAV_SIL_Get_PopsTotal
	text = {
		trigger = {
			check_variable = { PAV_SIL_General_Pop > PAV_RAPP_General_Pop }
		}
		localization_key = "PAV_SIL_General_Pop_G"
	}
	text = {
		localization_key = "PAV_SIL_General_Pop_Y"
	}
}

defined_text = {
	name = PAV_RAPP_Get_AuthTotal
	text = {
		trigger = {
		}
		localization_key = "PAV_RAPP_General_Auth_G"
	}
	text = {
		localization_key = "PAV_RAPP_General_Auth_Y"
	}
}

defined_text = {
	name = PAV_SIL_Get_AuthTotal
	text = {
		trigger = {
		}
		localization_key = "PAV_SIL_General_Auth_G"
	}
	text = {
		localization_key = "PAV_SIL_General_Auth_Y"
	}
}

defined_text = {
	name = PAV_RAPP_Get_Upper_House_seats
	text = {
		trigger = {
			check_variable = { PAV_RAPP_upper_seats > PAV_SIL_upper_seats }
		}
		localization_key = "PAV_RAPP_upper_seats_tt_G"
	}
	text = {
		trigger = {
			OR = {
				NOT = { has_country_flag = Modernist_Constitution }
			}
		}
		localization_key = "PAV_RAPP_upper_seats_tt_Y"
	}
}

defined_text = {
	name = PAV_SIL_Get_Upper_House_Seats
	text = {
		trigger = {
			check_variable = { PAV_SIL_upper_seats > PAV_RAPP_upper_seats }
		}
		localization_key = "PAV_SIL_upper_seats_tt_G"
	}
	text = {
		trigger = {
			OR = {
				NOT = { has_country_flag = Modernist_Constitution }
			}
		}
		localization_key = "PAV_SIL_upper_seats_tt_Y"
	}
}

defined_text = {
	name = PAV_RAPP_Get_Lower_House_Seats
	text = {
		trigger = {
			check_variable = { PAV_RAPP_lower_seats > PAV_SIL_lower_seats }
		}
		localization_key = "PAV_RAPP_lower_seats_tt_G"
	}
	text = {
		trigger = {
			NOT = { has_country_flag = Modernist_Constitution }
		}
		localization_key = "PAV_RAPP_lower_seats_tt_Y"
	}
}

defined_text = {
	name = PAV_RAPP_Get_Lower_House_Seats_Event
	text = {
		trigger = {
			NOT = { has_country_flag = Modernist_Constitution }
		}
		localization_key = "PAV_RAPP_lower_seats_tt"
	}
}

defined_text = {
	name = PAV_RAPP_Get_Upper_House_seats_Event
	text = {
		trigger = {
			OR = {
				NOT = { has_country_flag = Modernist_Constitution }
			}
		}
		localization_key = "PAV_RAPP_upper_seats_tt"
	}
}

defined_text = {
	name = PAV_SIL_Get_Lower_House_Seats
	text = {
		trigger = {
			#NOT = { has_country_flag = Modernist_Constitution }
			check_variable = { PAV_SIL_lower_seats > PAV_RAPP_lower_seats }
		}
		localization_key = "PAV_SIL_lower_seats_tt_G"
	}
	text = {
		trigger = {
			#NOT = { has_country_flag = Modernist_Constitution }
		}
		localization_key = "PAV_SIL_lower_seats_tt_Y"
	}
}

defined_text = {
	name = TNO_Federation_Presidential_Government_Support
	
	text = {
		trigger = {
			check_variable = { Presidential_Government_Support > 65 }
		}
		localization_key = "TNO_Federation_Presidential_Government_Support_tt_g"
	}
	text = {
		trigger = {
			check_variable = { Presidential_Government_Support < 35 }
		}
		localization_key = "TNO_Federation_Presidential_Government_Support_tt_r"
	}
	text = {
		trigger = {
			check_variable = { Presidential_Government_Support > 35 }
			check_variable = { Presidential_Government_Support < 65 }
		}
		localization_key = "TNO_Federation_Presidential_Government_Support_tt_y"
	}
}

defined_text = {
	name = PAV_Interaction_Available_Key
	
	text = {
		trigger = {
			has_country_flag = PAV_Presidential_Interaction
		}
		localization_key = "PAV_Interaction_Available_Key_tt_r"
	}
	
	text = {
		trigger = {
			NOT = {
				has_country_flag = PAV_Presidential_Interaction
			}
		}
		localization_key = "PAV_Interaction_Available_Key_tt_g"
	}
}