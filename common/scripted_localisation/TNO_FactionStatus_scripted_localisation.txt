### Image Locations
GetFactionStatus_icon = {
	name = GetFactionStatus_icon

	### Pakt ###
	#Master of Europe
	text = {
		trigger = { has_idea = Pakt_Leader }
		localization_key = Pakt_Leader_displaykey
	}
	#Militärverwaltung
	text = {
		trigger = { has_idea = Pakt_Military_Authority }
		localization_key = Pakt_Military_Authority_displaykey
	}
	#Protectorate
	text = {
		trigger = { has_idea = Pakt_Protektorat }
		localization_key = Pakt_Protektorat_displaykey
	}
	#Integrated Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Integrated_Reichskommissariat }
		localization_key = Pakt_Integrated_Reichskommissariat_displaykey
	}
	#Autonomous Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Autonomous_Reichskommissariat }
		localization_key = Pakt_Autonomous_Reichskommissariat_displaykey
	}
	#Reichsland
	text = {
		trigger = { has_idea = Pakt_Reichsland }
		localization_key = Pakt_Reichsland_displaykey
	}
	#Puppet State
	text = {
		trigger = { has_idea = Pakt_Marionettenstaat }
		localization_key = Pakt_Marionettenstaat_displaykey
	}
	#Collaborating Member
	text = {
		trigger = { has_idea = Pakt_Mitstreiter }
		localization_key = Pakt_Mitstreiter_displaykey
	}
	#Sovereign Member
	text = {
		trigger = { has_idea = Pakt_Bundnispartner }
		localization_key = Pakt_Bundnispartner_displaykey
	}
	#Ordensstaat
	text = {
		trigger = { has_idea = Pakt_Ordensstaat }
		localization_key = Pakt_Ordensstaat_displaykey
	}
	#Observer
	text = {
		trigger = { has_idea = Pakt_Observer }
		localization_key = Pakt_Observer_displaykey
	}

	text = {
		trigger = { has_idea = Anti_Heydrich_Pact }
		localization_key = Anti_Heydrich_Pact_displaykey
	}

	text = {
		trigger = { has_idea = Anti_Himmler }
		localization_key = Anti_Himmler_displaykey
	}

	text = {
		trigger = { has_idea = Anti_Heydrich }
		localization_key = Anti_Heydrich_displaykey
	}

	### Sphere ###
	#Leader
	text = {
		trigger = { has_idea = Sphere_Leader }
		localization_key = Sphere_Leader_displaykey
	}
	#Special Economic Zone
	text = {
		trigger = { has_idea = Sphere_Special_Econ_Zone }
		localization_key = Sphere_Special_Econ_Zone_displaykey
	}
	#Imperial Protectorate
	text = {
		trigger = { has_idea = Sphere_Imp_Protectorate }
		localization_key = Sphere_Imp_Protectorate_displaykey
	}
	#Fully Dependent
	text = {
		trigger = { has_idea = Sphere_Fully_Dependent }
		localization_key = Sphere_Fully_Dependent_displaykey
	}
	#Economically Dependent
	text = {
		trigger = { has_idea = Sphere_Economic_Dependent }
		localization_key = Sphere_Economic_Dependent_displaykey
	}
	#CoEqual Member
	text = {
		trigger = { has_idea = Sphere_Coequal }
		localization_key = Sphere_Coequal_displaykey
	}

	### OFN ###
	text = {
		trigger = { has_idea = OFN_Leader_of_The_Free_World }
		localization_key = OFN_Leader_of_The_Free_World_displaykey
	}
	text = {
		trigger = { has_idea = OFN_Independent_Member }
		localization_key = OFN_Independent_Member_displaykey
	}
	text = {
		trigger = { has_idea = OFN_Dependent_Member }
		localization_key = OFN_Dependent_Member_displaykey
	}
	text = {
		trigger = { has_idea = OFN_Military_Government }
		localization_key = OFN_Military_Government_displaykey
	}
	text = {
		trigger = { has_idea = OFN_Mandate }
		localization_key = OFN_Mandate_displaykey
	}

	#Observer
	text = {
		trigger = { has_idea = OFN_Observer }
		localization_key = "GFX_OFN_Observer"
	}
	
	##CSTO##
	#Guardian of Eurasia
	text = {
		trigger = { has_idea = CSTO_Guardian_of_Eurasia }
		localization_key = "GFX_CSTO_Member"
	}
	
	text = {
		trigger = { has_idea = CSTO_Member }
		localization_key = "GFX_CSTO_Member"
	}
	
	#Observer
	text = {
		trigger = { has_idea = CSTO_Observer }
		localization_key = "GFX_CSTO_Observer"
	}
	
	### TRIUM ###
	text = {
		trigger = { has_idea = TRI_Founder_IT }
		localization_key = TRI_Founder_IT_displaykey
	}
	text = {
		trigger = { has_idea = TRI_Founder_IB }
		localization_key = TRI_Founder_IB_displaykey
	}
	text = {
		trigger = { has_idea = TRI_Founder_TR }
		localization_key = TRI_Founder_TR_displaykey
	}
	text = {
		trigger = { has_idea = TRI_Member }
		localization_key = TRI_Member_displaykey
	}
	text = {
		trigger = { has_idea = TRI_Member_Client }
		localization_key = TRI_Member_Client_displaykey
	}

	#
	text = {
		localization_key = NoFaction
	}
}

##TOOLTIP
#Tooltip Template
GetFactionStatus_tooltip = {
	name = GetFactionStatus_tooltip
	text = { localization_key = FactionStatus_tooltip }
}

#Name
GetFactionStatus_name = {
	name = GetFactionStatus_name

	### Pakt ###
	#Master of Europe
	text = {
		trigger = { has_idea = Pakt_Leader }
		localization_key = Pakt_Leader
	}
	#Militärverwaltung
	text = {
		trigger = { has_idea = Pakt_Military_Authority }
		localization_key = Pakt_Military_Authority
	}
	#Protectorate
	text = {
		trigger = { has_idea = Pakt_Protektorat }
		localization_key = Pakt_Protektorat
	}
	#Integrated Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Integrated_Reichskommissariat }
		localization_key = Pakt_Integrated_Reichskommissariat
	}
	#Autonomous Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Autonomous_Reichskommissariat }
		localization_key = Pakt_Autonomous_Reichskommissariat
	}
	#Reichsland
	text = {
		trigger = { has_idea = Pakt_Reichsland }
		localization_key = Pakt_Reichsland
	}
	#Puppet State
	text = {
		trigger = { has_idea = Pakt_Marionettenstaat }
		localization_key = Pakt_Marionettenstaat
	}
	#Collaborating Member
	text = {
		trigger = { has_idea = Pakt_Mitstreiter }
		localization_key = Pakt_Mitstreiter
	}
	#Sovereign Member
	text = {
		trigger = { has_idea = Pakt_Bundnispartner }
		localization_key = Pakt_Bundnispartner
	}
	#Ordensstaat
	text = {
		trigger = { has_idea = Pakt_Ordensstaat }
		localization_key = Pakt_Ordensstaat
	}
	#Observer
	text = {
		trigger = { has_idea = Pakt_Observer }
		localization_key = Pakt_Observer
	}

	text = {
		trigger = { has_idea = Anti_Heydrich_Pact }
		localization_key = Anti_Heydrich_Pact
	}

	text = {
		trigger = { has_idea = Anti_Himmler }
		localization_key = Anti_Himmler
	}

	text = {
		trigger = { has_idea = Anti_Heydrich }
		localization_key = Anti_Heydrich
	}

	### Sphere ###
	#Leader
	text = {
		trigger = { has_idea = Sphere_Leader }
		localization_key = Sphere_Leader
	}
	#Special Economic Zone
	text = {
		trigger = { has_idea = Sphere_Special_Econ_Zone }
		localization_key = Sphere_Special_Econ_Zone
	}
	#Imperial Protectorate
	text = {
		trigger = { has_idea = Sphere_Imp_Protectorate }
		localization_key = Sphere_Imp_Protectorate
	}
	#Fully Dependent
	text = {
		trigger = { has_idea = Sphere_Fully_Dependent }
		localization_key = Sphere_Fully_Dependent
	}
	#Economically Dependent
	text = {
		trigger = { has_idea = Sphere_Economic_Dependent }
		localization_key = Sphere_Economic_Dependent
	}
	#CoEqual Member
	text = {
		trigger = { has_idea = Sphere_Coequal }
		localization_key = Sphere_Coequal
	}
	text = {
		trigger = { has_idea = OFN_Leader_of_The_Free_World }
		localization_key = OFN_Leader_of_The_Free_World
	}
	text = {
		trigger = { has_idea = OFN_Independent_Member }
		localization_key = OFN_Independent_Member
	}
	text = {
		trigger = { has_idea = OFN_Dependent_Member }
		localization_key = OFN_Dependent_Member
	}
	text = {
		trigger = { has_idea = OFN_Military_Government }
		localization_key = OFN_Military_Government
	}
	text = {
		trigger = { has_idea = OFN_Mandate }
		localization_key = OFN_Mandate
	}

	#Observer
	text = {
		trigger = { has_idea = OFN_Observer }
		localization_key = OFN_Observer
	}
	
	##CSTO##
	#Guardian of Eurasia
	text = {
		trigger = { has_idea = CSTO_Guardian_of_Eurasia }
		localization_key = CSTO_Guardian_of_Eurasia
	}
	
	text = {
		trigger = { has_idea = CSTO_Member }
		localization_key = CSTO_Member
	}
	
	#Observer
	text = {
		trigger = { has_idea = CSTO_Observer }
		localization_key = CSTO_Observer
	}
	
	text = {
		trigger = { has_idea = TRI_Founder_IT }
		localization_key = TRI_Founder_IT
	}
	text = {
		trigger = { has_idea = TRI_Founder_IB }
		localization_key = TRI_Founder_IB
	}
	text = {
		trigger = { has_idea = TRI_Founder_TR }
		localization_key = TRI_Founder_TR
	}
	text = {
		trigger = { has_idea = TRI_Member }
		localization_key = TRI_Member
	}
	text = {
		trigger = { has_idea = TRI_Member_Client }
		localization_key = TRI_Member_Client
	}
}

#Name
GetFactionStatus_effect = {
	name = GetFactionStatus_effect

	### Pakt ###
	#Master of Europe
	text = {
		trigger = { has_idea = Pakt_Leader }
		localization_key = Pakt_Leader_effect
	}
	#Militärverwaltung
	text = {
		trigger = { has_idea = Pakt_Military_Authority }
		localization_key = Pakt_Military_Authority_effect
	}
	#Protectorate
	text = {
		trigger = { has_idea = Pakt_Protektorat }
		localization_key = Pakt_Protektorat_effect
	}
	#Integrated Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Integrated_Reichskommissariat }
		localization_key = Pakt_Integrated_Reichskommissariat_effect
	}
	#Autonomous Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Autonomous_Reichskommissariat }
		localization_key = Pakt_Autonomous_Reichskommissariat_effect
	}
	#Autonomous Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Reichsland }
		localization_key = Pakt_Reichsland_effect
	}
	#Puppet State
	text = {
		trigger = { has_idea = Pakt_Marionettenstaat }
		localization_key = Pakt_Marionettenstaat_effect
	}
	#Collaborating Member
	text = {
		trigger = { has_idea = Pakt_Mitstreiter }
		localization_key = Pakt_Mitstreiter_effect
	}
	#Sovereign Member
	text = {
		trigger = { has_idea = Pakt_Bundnispartner }
		localization_key = Pakt_Bundnispartner_effect
	}
	#Ordensstaat
	text = {
		trigger = { has_idea = Pakt_Ordensstaat }
		localization_key = Pakt_Ordensstaat_effect
	}
	#Observer
	text = {
		trigger = { has_idea = Pakt_Observer }
		localization_key = Pakt_Observer_effect
	}

	text = {
		trigger = { has_idea = Anti_Heydrich_Pact }
		localization_key = Anti_Heydrich_Pact_effect
	}

	text = {
		trigger = { has_idea = Anti_Himmler }
		localization_key = Anti_Himmler_effect
	}

	text = {
		trigger = { has_idea = Anti_Heydrich }
		localization_key = Anti_Heydrich_effect
	}

	### Sphere ###
	#Leader
	text = {
		trigger = { has_idea = Sphere_Leader }
		localization_key = Sphere_Leader_effect
	}
	#Special Economic Zone
	text = {
		trigger = { has_idea = Sphere_Special_Econ_Zone }
		localization_key = Sphere_Special_Econ_Zone_effect
	}
	#Imperial Protectorate
	text = {
		trigger = { has_idea = Sphere_Imp_Protectorate }
		localization_key = Sphere_Imp_Protectorate_effect
	}
	#Fully Dependent
	text = {
		trigger = { has_idea = Sphere_Fully_Dependent }
		localization_key = Sphere_Fully_Dependent_effect
	}
	#Economically Dependent
	text = {
		trigger = { has_idea = Sphere_Economic_Dependent }
		localization_key = Sphere_Economic_Dependent_effect
	}
	#CoEqual Member
	text = {
		trigger = { has_idea = Sphere_Coequal }
		localization_key = Sphere_Coequal_effect
	}

	### OFN ###
	text = {
		trigger = { has_idea = OFN_Leader_of_The_Free_World }
		localization_key = OFN_Leader_of_The_Free_World_effect
	}
	text = {
		trigger = { has_idea = OFN_Independent_Member }
		localization_key = OFN_Independent_Member_effect
	}
	text = {
		trigger = { has_idea = OFN_Dependent_Member }
		localization_key = OFN_Dependent_Member_effect
	}
	text = {
		trigger = { has_idea = OFN_Military_Government }
		localization_key = OFN_Military_Government_effect
	}
	text = {
		trigger = { has_idea = OFN_Mandate }
		localization_key = OFN_Mandate_effect
	}
	#Observer
	text = {
		trigger = { has_idea = OFN_Observer }
		localization_key = OFN_Observer_effect
	}
	
	##CSTO##
	#Guardian of Eurasia
	text = {
		trigger = { has_idea = CSTO_Guardian_of_Eurasia }
		localization_key = CSTO_Guardian_of_Eurasia_effect
	}
	
	text = {
		trigger = { has_idea = CSTO_Member }
		localization_key = CSTO_Member_effect
	}
	
	#Observer
	text = {
		trigger = { has_idea = CSTO_Observer }
		localization_key = CSTO_Observer_effect
	}
}

GetFactionStatus_desc = {
	name = GetFactionStatus_desc

	### Pakt ###
	#Master of Europe
	text = {
		trigger = { has_idea = Pakt_Leader }
		localization_key = Pakt_Leader_desc
	}
	#Militärverwaltung
	text = {
		trigger = { has_idea = Pakt_Military_Authority }
		localization_key = Pakt_Military_Authority_desc
	}
	#Protectorate
	text = {
		trigger = { has_idea = Pakt_Protektorat }
		localization_key = Pakt_Protektorat_desc
	}
	#Integrated Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Integrated_Reichskommissariat }
		localization_key = Pakt_Integrated_Reichskommissariat_desc
	}
	#Autonomous Reichskommissariat
	text = {
		trigger = { has_idea = Pakt_Autonomous_Reichskommissariat }
		localization_key = Pakt_Autonomous_Reichskommissariat_desc
	}
	#Reichsland
	text = {
		trigger = { has_idea = Pakt_Reichsland }
		localization_key = Pakt_Reichsland_desc
	}
	#Puppet State
	text = {
		trigger = { has_idea = Pakt_Marionettenstaat }
		localization_key = Pakt_Marionettenstaat_desc
	}
	#Collaborating Member
	text = {
		trigger = { has_idea = Pakt_Mitstreiter }
		localization_key = Pakt_Mitstreiter_desc
	}
	#Sovereign Member
	text = {
		trigger = { has_idea = Pakt_Bundnispartner }
		localization_key = Pakt_Bundnispartner_desc
	}
	#Ordensstaat
	text = {
		trigger = { has_idea = Pakt_Ordensstaat }
		localization_key = Pakt_Ordensstaat_desc
	}
	#Observer
	text = {
		trigger = { has_idea = Pakt_Observer }
		localization_key = Pakt_Observer_desc
	}

	text = {
		trigger = { has_idea = Anti_Heydrich_Pact }
		localization_key = Anti_Heydrich_Pact_desc
	}

	text = {
		trigger = { has_idea = Anti_Himmler }
		localization_key = Anti_Himmler_desc
	}

	text = {
		trigger = { has_idea = Anti_Heydrich }
		localization_key = Anti_Heydrich_desc
	}

	### Sphere ###
	#Leader
	text = {
		trigger = { has_idea = Sphere_Leader }
		localization_key = Sphere_Leader_desc
	}
	#Special Economic Zone
	text = {
		trigger = { has_idea = Sphere_Special_Econ_Zone }
		localization_key = Sphere_Special_Econ_Zone_desc
	}
	#Imperial Protectorate
	text = {
		trigger = { has_idea = Sphere_Imp_Protectorate }
		localization_key = Sphere_Imp_Protectorate_desc
	}
	#Fully Dependent
	text = {
		trigger = { has_idea = Sphere_Fully_Dependent }
		localization_key = Sphere_Fully_Dependent_desc
	}
	#Economically Dependent
	text = {
		trigger = { has_idea = Sphere_Economic_Dependent }
		localization_key = Sphere_Economic_Dependent_desc
	}
	#CoEqual Member
	text = {
		trigger = { has_idea = Sphere_Coequal }
		localization_key = Sphere_Coequal_desc
	}
	### OFN ###
	text = {
		trigger = { has_idea = OFN_Leader_of_The_Free_World }
		localization_key = OFN_Leader_of_The_Free_World_desc
	}
	text = {
		trigger = { has_idea = OFN_Independent_Member }
		localization_key = OFN_Independent_Member_desc
	}
	text = {
		trigger = { has_idea = OFN_Dependent_Member }
		localization_key = OFN_Dependent_Member_desc
	}
	text = {
		trigger = { has_idea = OFN_Military_Government }
		localization_key = OFN_Military_Government_desc
	}
	text = {
		trigger = { has_idea = OFN_Mandate }
		localization_key = OFN_Mandate_desc
	}
	#Observer
	text = {
		trigger = { has_idea = OFN_Observer }
		localization_key = OFN_Observer_desc
	}
	
	##CSTO##
	#Guardian of Eurasia
	text = {
		trigger = { has_idea = CSTO_Guardian_of_Eurasia }
		localization_key = CSTO_Guardian_of_Eurasia_desc
	}
	
	text = {
		trigger = { has_idea = CSTO_Member }
		localization_key = CSTO_Member_desc
	}
	
	#Observer
	text = {
		trigger = { has_idea = CSTO_Observer }
		localization_key = CSTO_Observer_desc
	}
}
