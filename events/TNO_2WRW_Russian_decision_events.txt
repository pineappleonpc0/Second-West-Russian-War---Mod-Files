add_namespace = vyborg_border_conflict

country_event = { #Intro event
	id = vyborg_border_conflict.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event vyborg_border_conflict.1"}
	title = vyborg_border_conflict.1.t
	desc = vyborg_border_conflict.1.desc
	picture = GFX_report_event_soviet_russian_mech_infantry_6

	is_triggered_only = yes
	fire_only_once = yes

	option = { #
		name = vyborg_border_conflict.1.a
		ai_chance = { factor = 80 }
		start_border_war = {
			change_state_after_war = no
			attacker = {
				state = 195
				num_provinces = 2
				on_win = vyborg_border_conflict.2
				on_lose = vyborg_border_conflict.3
			}
						
			defender = {
				state = 146
				num_provinces = 2
			}
		}
	}
	
	option = { #
		name = vyborg_border_conflict.1.b
		ai_chance = { factor = 20 }
	}
}

country_event = { #Victory
	id = vyborg_border_conflict.2
	immediate = {log = "[GetDateText]: [Root.GetName]: event vyborg_border_conflict.2"}
	title = vyborg_border_conflict.2.t
	desc = vyborg_border_conflict.2.desc
	picture = GFX_report_event_russian_military

	is_triggered_only = yes
	fire_only_once = yes

	option = { #
		name = vyborg_border_conflict.2.a
		ai_chance = { factor = 1 }
		transfer_state = 146
		add_state_core = 146
	}
}

country_event = { #Victory
	id = vyborg_border_conflict.3
	immediate = {log = "[GetDateText]: [Root.GetName]: event vyborg_border_conflict.3"}
	title = vyborg_border_conflict.3.t
	desc = vyborg_border_conflict.3.desc
	picture = GFX_report_event_russia_devastation_generic_4

	is_triggered_only = yes
	fire_only_once = yes

	option = { #
		name = vyborg_border_conflict.3.a
		ai_chance = { factor = 1 }
		195 = {
			set_demilitarized_zone = yes
		}
	}
}

add_namespace = RUS_decisions

country_event = { #Move Capitals
	id = RUS_decisions.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event RUS_decisions.1"}
	title = RUS_decisions.1.t
	desc = RUS_decisions.1.desc
	picture = GFX_report_event_russia_devastation_generic_4

	is_triggered_only = yes
	fire_only_once = yes

	option = { #
		name = RUS_decisions.1.a
		ai_chance = { factor = 1 }
	}
	
	option = { #
		name = RUS_decisions.1.b
		ai_chance = { factor = 1 }
		trigger = {
			RUS = { owns_state = 219 }
		}
		set_capital = 219
	}
	
	option = { #
		name = RUS_decisions.1.c
		ai_chance = { factor = 1 }
		trigger = {
			RUS = { owns_state = 195 }
		}
		set_capital = 195
	}
	
	option = { #
		name = RUS_decisions.1.d
		ai_chance = { factor = 1 }
		trigger = {
			original_tag = KEM
		}
		set_capital = 202
	}
}